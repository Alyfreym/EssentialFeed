//
//  RemoteFeedLoaderTest.swift
//  EssentialFeedTests
//
//  Created by Alisher Shermatov on 08.02.2023.
//

import XCTest
import EssentialFeed


class HTTPClientSpy: HTTPClient {
    func get(from url: URL) {
        self.requestedURL = url
    }
    
    var requestedURL: URL?
}


class RemoteFeedLoaderTests: XCTestCase {
    
    func test_initDoesNotRequestDataFromURL() {
        
        let client = HTTPClientSpy()
        _ = RemoteFeedLoader(url: URL.init(string: "http://google.com")!, client: client)
        
        XCTAssertNil(client.requestedURL)
    }
    
    func test_requestDataFromURL() {
        let client = HTTPClientSpy()
        let sut = RemoteFeedLoader(url: URL.init(string: "http://mamasita.com")!, client: client)
        
        sut.load()
        
        XCTAssertNotNil(client.requestedURL)
    }
    
    private func makeSUT(url: URL = URL(string: "http://ssss.com")!, client: HTTPClient) -> RemoteFeedLoader {
        return RemoteFeedLoader(url: url, client: client)
    }
}
