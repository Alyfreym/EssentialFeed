//
//  RemoteFeedLoader.swift
//  EssentialFeed
//
//  Created by Alisher Shermatov on 14.02.2023.
//

import Foundation

public protocol HTTPClient {
    func get(from url: URL)
}


public final class RemoteFeedLoader {
    private let client: HTTPClient
    private let url: URL
    
    public init(url: URL, client: HTTPClient) {
        self.client = client
        self.url = url
    }
    
    public func load() {
        self.client.get(from: self.url)
    }
}
