//
//  FeedItem.swift
//  EssentialFeed
//
//  Created by Alisher Shermatov on 08.02.2023.
//

import Foundation


struct FeedItem {
    let id: UUID
    let description: String?
    let location: String?
    let imageURL: URL
}
